import { FunctionComponent } from "preact";
import cardDataJson from "../Home/homepageCardData.json";
import useRenderVideoEmbed from "../../hooks/useRenderVideoEmbed";
import styles from "../Modal/Modal.module.scss";

const WiresharkUContent: FunctionComponent = () => {
    const wiresharkUData = cardDataJson.filter(cardData => (cardData.title.includes("University")))

    const renderData = (data) => {
        return data.map((content) => {
            const { link, links, linkEmbed, name } = content;
        
            const { renderVideoEmbed } = useRenderVideoEmbed(
            linkEmbed,
            "100%",
            "250px"
            );
        
            return (
            <>
                {!!link ? (
                <a href={link}>
                    <h4>{content.name}</h4>
                </a>
                ) : (
                <h4 class={styles.h4Black}>{name}</h4>
                )}
                {linkEmbed && renderVideoEmbed}
                {links && (
                <ul>
                    {links.map((linkData) => {
                    return (
                        <a href={linkData.link}>
                        <li>{linkData.title}</li>
                        </a>
                    );
                    })}
                </ul>
                )}
            </>
            );
        });
    }

    return (
        <>
            {wiresharkUData.map((wiresharkU) => (
                <div id={wiresharkU.title.split(" ").join("")}>
                    <h2>{wiresharkU.title.toLocaleUpperCase()}</h2>
                    <h4>{wiresharkU.subtitle}</h4>
                    <div>{renderData(wiresharkU.modalData)}</div>
                </div>
            ))}
        </>
    )
}

export default WiresharkUContent