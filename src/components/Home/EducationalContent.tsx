import styles from "./Homepage.module.scss";
import Card from "../Card/Card";
import cardDataJson from "./homepageCardData.json";

const EducationalContent: preact.FunctionComponent = () => {
  const renderCards = () => {
    return cardDataJson?.map((cardInfo, index) => {
      if (cardInfo.title === "Wireshark 4.0 Overview") {
        return (
          <Card
            id={cardInfo.id}
            key={index}
            title={cardInfo.title}
            subtitle={cardInfo.subtitle}
            imgUrl={cardInfo.imgUrl}
            link={cardInfo.link}
            videoUrl={cardInfo.videoUrl}
            shouldLinkToModal={cardInfo.shouldLinkToModal}
            modalData={cardInfo.modalData}
          />
        )
      }

      return (
        <Card
          key={index}
          title={cardInfo.title}
          subtitle={cardInfo.subtitle}
          imgUrl={cardInfo.imgUrl}
          link={cardInfo.link}
          videoUrl={cardInfo.videoUrl}
          shouldLinkToModal={cardInfo.shouldLinkToModal}
          modalData={cardInfo.modalData}
        />
      );
    });
  };

  return (
    <section id="educationalContent" className={styles.educationalContent}>
      <div className={styles.cardsContainer}>{renderCards()}</div>
    </section>
  );
};

export default EducationalContent;
