---
sticky: 0
headline: "Wireshark 4.0.0rc1 Release Candidate"
# xmllint --html --xpath '//div/h2[@id="_whats_new"]/..' docbook/release-notes.html | sed -e 's/^/    /' -e 's/\\/\&bsol;/g' | pbcopy
body: |
    <div class="sect1">
    <h2 id="_whats_new">What’s New</h2>
    <div class="sectionbody">
    <div class="admonitionblock note">
    <table>
    <tr>
    <td class="icon">
    <div class="title">Note</div>
    </td>
    <td class="content">
    We do not ship official 32-bit Windows packages for this branch.
    If you need to use Wireshark on that platform, please install the latest 3.6 release.
    <a href="https://gitlab.com/wireshark/wireshark/-/issues/17779">Issue 17779</a>
    </td>
    </tr>
    </table>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>The display filter syntax is now more powerful with many new extensions.
    See below for details.</p>
    </li>
    <li>
    <p>The Conversation and Endpoint dialogs have been redesigned with the following improvements:</p>
    <div class="ulist">
    <ul>
    <li>
    <p>The context menu now includes the option to resize all columns, as well as copying elements.</p>
    </li>
    <li>
    <p>Data may be exported as JSON.</p>
    </li>
    <li>
    <p>Tabs may be detached and reattached from the dialog.</p>
    </li>
    <li>
    <p>Adding and removing tabs will keep them in the same order all the time.</p>
    </li>
    <li>
    <p>If a filter is applied, two columns are shown in either dialog detailing the difference between
    unmatched and matched packets.</p>
    </li>
    <li>
    <p>Columns are now sorted via secondary properties if an identical entry is found.</p>
    </li>
    <li>
    <p>Conversations are sorted via second address and first port number.</p>
    </li>
    <li>
    <p>Endpoints are sorted via port numbers.</p>
    </li>
    <li>
    <p>IPv6 addresses are sorted correctly after IPv4 addresses.</p>
    </li>
    <li>
    <p>The dialog elements have been moved to make it easier to handle for new users.</p>
    </li>
    <li>
    <p>Selection of tap elements is done via a list.</p>
    </li>
    <li>
    <p>All configurations and options are done via a left side button row.</p>
    </li>
    <li>
    <p>Columns for the Conversations and Endpoint dialogs can be hidden by a context menu.</p>
    </li>
    <li>
    <p>TCP and UDP conversations now include the stream ID and allow filtering on it.</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>Speed when using MaxMind geolocation has been greatly improved.</p>
    </li>
    <li>
    <p>The Wireshark Lua API now uses the <a href="https://github.com/rrthomas/lrexlib">lrexlib</a> bindings to PCRE2.
    Code using the Lua GRegex module will have to be updated to use lrexlib-pcre2 instead.
    In most cases the API should be compatible and the conversion just requires a module name change.</p>
    </li>
    <li>
    <p>The tap registration system has been updated and the list of arguments for tap_packet_cb has changed.
    All taps registered through register_tap_listener have to be updated.</p>
    </li>
    <li>
    <p>The <a href="https://www.pcre.org/">PCRE2 library</a> is now a required dependency to build Wireshark.</p>
    </li>
    <li>
    <p>You must now have a compiler with C11 support in order to build Wireshark.</p>
    </li>
    <li>
    <p>The following libraries and tools have had their minimum required version increased:</p>
    <div class="ulist">
    <ul>
    <li>
    <p>CMake 3.10 is required on macOS and Linux.</p>
    </li>
    <li>
    <p>Qt version 5.12 (was 5.6.0), although compilation with 5.10 and 5.11 is still possible, but will trigger a warning during configuration.</p>
    </li>
    <li>
    <p>Windows SDK 10.0.18362.0 is required due to issues with C11 support.</p>
    </li>
    <li>
    <p>macOS version 10.10 (was 10.8) is required, if the Qt version is to be built, at least 10.11 is required, depending on the Qt version used (see below).</p>
    </li>
    <li>
    <p>GLib version 2.50.0 (was 2.38.0) is required.</p>
    </li>
    <li>
    <p>Libgcrypt version 1.8.0 (was 1.5.0) is required.</p>
    </li>
    <li>
    <p>c-ares version 1.14.0 (was 1.5.0).</p>
    </li>
    <li>
    <p>Python version 3.6.0 (was 3.4.0).</p>
    </li>
    <li>
    <p>GnuTLS version 3.5.8 (was 3.3.0).</p>
    </li>
    <li>
    <p>Nghttp2 minimum version has been set to 1.11.0 (none previous).</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>For building with Qt on macOS, the following versions are required depending on the Qt version to be used:</p>
    <div class="ulist">
    <ul>
    <li>
    <p>Qt 5.10 or higher requires macOS version 10.11</p>
    </li>
    <li>
    <p>Qt 5.12 or higher requires macOS version 10.12</p>
    </li>
    <li>
    <p>Qt 5.14 or higher requires macOS version 10.13</p>
    </li>
    <li>
    <p>Qt 6.0 or higher requires macOS version 10.14</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>Perl is no longer required to build Wireshark, but may be required to build some source code files and run code analysis checks.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>Many other improvements have been made.
    See the “New and Updated Features” section below for more details.</p>
    </div>
    <div class="sect2">
    <h3 id="_new_and_updated_features">New and Updated Features</h3>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated) since version 3.7.2:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>The Windows installers now ship with Npcap 1.70.
    They previously shipped with Npcap 1.60.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated) since version 3.7.1:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>The 'v' (lower case) and 'V' (upper case) switches have been swapped for editcap and mergecap to
    match the other command line utilities.</p>
    </li>
    <li>
    <p>The ip.flags field is now only the three high bits, not the full byte.
    Display filters and Coloring rules using the field will need to be adjusted.</p>
    </li>
    <li>
    <p>New address type AT_NUMERIC allows simple numeric addresses for protocols which do not have
    a more common-style address approach, analog to AT_STRINGZ.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated) since version 3.7.0:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>The Windows installers now ship with Qt 6.2.3.
    They previously shipped with Qt 6.2.4.</p>
    </li>
    <li>
    <p>The Conversation and Endpoint dialogs have been reworked extensively</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated) since version 3.6.0:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>The Windows installers now ship with Npcap 1.60.
    They previously shipped with Npcap 1.55.</p>
    </li>
    <li>
    <p>The Windows installers now ship with Qt 6.2.4.
    They previously shipped with Qt 5.12.2.</p>
    </li>
    <li>
    <p>The display filter syntax has been updated and enhanced:</p>
    <div class="ulist">
    <ul>
    <li>
    <p>A syntax to match a specific layer in the protocol stack has been added.
    For example in an IP-over-IP packet “ip.addr#1 == 1.1.1.1” matches the outer layer addresses and “ip.addr#2 == 1.1.1.2” matches the inner layer addresses.</p>
    </li>
    <li>
    <p>Universal quantifiers "any" and "all" have been added to any relational operator.
    For example the expression <span class="menuseq"><b class="menu">all tcp.port</b> <b class="caret">›</b> <b class="menuitem">1024</b></span> is true if and only if all tcp.port fields match the condition.
    Previously only the default behaviour to return true if any one field matches was supported.</p>
    </li>
    <li>
    <p>Field references, of the form ${some.field}, are now part of the syntax of display filters. Previously they were implemented as macros.
    The new implementation is more efficient and has the same properties as protocol fields, like matching on multiple values
    using quantifiers and support for layer filtering.</p>
    </li>
    <li>
    <p>Arithmetic is supported for numeric fields with the usual operators “+”, “-”, “*”, “/”, and “%”.
    Arithmetic expressions must be grouped using curly brackets (not parenthesis).</p>
    </li>
    <li>
    <p>New display filter functions max(), min() and abs() have been added.</p>
    </li>
    <li>
    <p>Functions can accept expressions as arguments, including other functions.
    Previously only protocol fields and slices were syntactically valid function arguments.</p>
    </li>
    <li>
    <p>A new syntax to disambiguate literals from identifiers has been added.
    Every value with a leading dot is a protocol or protocol field.
    Every value in between angle brackets is a literal value.
    See the <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChWorkBuildDisplayFilterSection.html#_some_protocol_names_can_be_ambiguous">User’s Guide</a> for details.</p>
    </li>
    <li>
    <p>The "bitwise and" operator is now a first-class bit operator, not a boolean operator.
    In particular this means it is now possible to mask bits, e.g.: frame[0] &amp; 0x0F == 3.</p>
    </li>
    <li>
    <p>Dates and times can be given in UTC using ISO 8601 (with 'Z' timezone) or by appending the suffix "UTC" to the legacy formats.
    Otherwise local time is used.</p>
    </li>
    <li>
    <p>Integer literal constants may be written in binary (in addition to decimal/octal/hexadecimal) using the prefix "0b" or "0B".</p>
    </li>
    <li>
    <p>Logical AND now has higher precedence than logical OR, in line with most programming languages.</p>
    </li>
    <li>
    <p>It is now possible to index protocol fields from the end using negative indexes. For example the
    following expression tests the last two bytes of the TCP protocol field: tcp[-2:] == AA:BB.
    This was a longstanding bug that has been fixed in this release.</p>
    </li>
    <li>
    <p>Set elements must be separated using a comma, e.g: {1, 2, "foo"}.
    Using only whitespace as a separator was deprecated in 3.6 and is now a syntax error.</p>
    </li>
    <li>
    <p>Support for some additional character escape sequences in double quoted strings has been added.
    Along with octal (&bsol;&lt;number&gt;) and hex (&bsol;x&lt;number&gt;) encoding, the following C escape sequences are now supported with the same meaning: &bsol;a, &bsol;b, &bsol;f, &bsol;n, &bsol;r, &bsol;t, &bsol;v.
    Previously they were only supported with character constants.</p>
    </li>
    <li>
    <p>Unicode universal character names are now supported with the escape sequences &bsol;uNNNN or &bsol;UNNNNNNNN, where N is a hexadecimal digit.</p>
    </li>
    <li>
    <p>Unrecognized escape sequences are now treated as a syntax error.
    Previously they were treated as a literal character.
    In addition to the sequences indicated above, backslash, single quotation and double quotation mark are also valid sequences: &bsol;&bsol;, &bsol;', &bsol;".</p>
    </li>
    <li>
    <p>A new strict equality operator "===" or "all_eq" has been added.
    The expression "a === b" is true if and only if all a’s are equal to b.
    The negation of "===" can now be written as "!==" (any_ne).</p>
    </li>
    <li>
    <p>The aliases "any_eq" for "==" and "all_ne" for "!=" have been added.</p>
    </li>
    <li>
    <p>The operator "~=" is deprecated and will be removed in a future version.
    Use "!==", which has the same meaning instead.</p>
    </li>
    <li>
    <p>Floats must be written with a leading and ending digit.
    For example the values ".7" and "7." are now invalid as floats.
    They must be written "0.7" and "7.0" respectively.</p>
    </li>
    <li>
    <p>The display filter engine now uses PCRE2 instead of GRegex (GLib’s bindings to the older and end-of-life PCRE library).
    PCRE2 is compatible with PCRE so any user-visible changes should be minimal.
    Some exotic patterns may now be invalid and require rewriting.</p>
    </li>
    <li>
    <p>Literal strings can handle embedded null bytes (the value '&bsol;0') correctly. This includes regular expression patterns.
    For example the double-quoted string "&bsol;0 is a null byte" is a legal literal value.
    This may be useful to match byte patterns but note that in general protocol fields with a string type still cannot contain embedded null bytes.</p>
    </li>
    <li>
    <p>Booleans can be written as True/TRUE or False/FALSE. Previously they could only be written as 1 or 0.</p>
    </li>
    <li>
    <p>It is now possible to test for the existence of a slice.</p>
    </li>
    <li>
    <p>All integer sizes are now compatible. Unless overflow occurs any integer field can be compared with any other.</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>The <code>text2pcap</code> command and the “Import from Hex Dump” feature have been updated and enhanced:</p>
    <div class="ulist">
    <ul>
    <li>
    <p><code>text2pcap</code> supports writing the output file in all the capture file formats that wiretap library supports, using the same <code>-F</code> option as <code>editcap</code>, <code>mergecap</code>, and <code>tshark</code>.</p>
    </li>
    <li>
    <p>Consistent with the other command line tools like <code>editcap</code>, <code>mergecap</code>, <code>tshark</code>, and the "Import from Hex Dump" option within Wireshark, the default capture file format for <code>text2pcap</code> is now <strong>pcapng</strong>. The <code>-n</code> flag to select pcapng (instead of the previous default, pcap) has been deprecated and will be removed in a future release.</p>
    </li>
    <li>
    <p><code>text2pcap</code> supports selecting the encapsulation type of the output file format using the wiretap library short names with an <code>-E</code> option, similar to the <code>-T</code> option of <code>editcap</code>.</p>
    </li>
    <li>
    <p><code>text2pcap</code> has been updated to use the new logging output options and the <code>-d</code> flag has been removed.
    The "debug" log level corresponds to the old <code>-d</code> flag, and the "noisy" log level corresponds to using <code>-d</code> multiple times.</p>
    </li>
    <li>
    <p><code>text2pcap</code> and “Import from Hex Dump” support writing fake IP, TCP, UDP, and SCTP headers to files with Raw IP, Raw IPv4, and Raw IPv6 encapsulations, in addition to Ethernet encapsulation available in previous versions.</p>
    </li>
    <li>
    <p><code>text2pcap</code> supports scanning the input file using a custom regular expression, as supported in “Import from Hex Dump” in Wireshark 3.6.x.</p>
    </li>
    <li>
    <p>In general, <code>text2pcap</code> and wireshark’s “Import from Hex Dump” have feature parity.</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>The default main window layout has been changed so that the Packet Detail and Packet Bytes are side by side underneath the Packet List pane.</p>
    </li>
    <li>
    <p>The HTTP2 dissector now supports using fake headers to parse the DATAs of streams captured without first HEADERS frames of a long-lived stream (such as a gRPC streaming call which allows sending many request or response messages in one HTTP2 stream).
    Users can specify fake headers using an existing stream’s server port, stream id and direction.</p>
    </li>
    <li>
    <p>The IEEE 802.11 dissector supports Mesh Connex (MCX).</p>
    </li>
    <li>
    <p>The “Capture Options” dialog contains the same configuration icon as the Welcome Screen.
    It is now possible to configure interfaces there.</p>
    </li>
    <li>
    <p>The “Extcap” dialog remembers password items during runtime, which makes it possible to run extcaps multiple times in row without having to reenter the password each time.
    Passwords are never stored on disk.</p>
    </li>
    <li>
    <p>It is possible to set extcap passwords in <code>tshark</code> and other CLI tools.</p>
    </li>
    <li>
    <p>The extcap configuration dialog now supports and remembers empty strings.
    There are new buttons to reset values back to their defaults.</p>
    </li>
    <li>
    <p>Support to display JSON mapping for Protobuf message has been added.</p>
    </li>
    <li>
    <p>macOS debugging symbols are now shipped in separate packages, similar to Windows packages.</p>
    </li>
    <li>
    <p>In the ZigBee ZCL Messaging dissector the zbee_zcl_se.msg.msg_ctrl.depreciated field has been renamed to zbee_zcl_se.msg.msg_ctrl.deprecated</p>
    </li>
    <li>
    <p>The interface list on the welcome page sorts active interfaces first and only displays sparklines for active interfaces.
    Additionally, the interfaces can now be hidden and shown via the context menu in the interface list</p>
    </li>
    <li>
    <p>The Event Tracing for Windows (ETW) file reader now supports displaying IP packets from an event trace logfile or an event trace live session.</p>
    </li>
    <li>
    <p>ciscodump now supports IOS, IOS-XE and ASA remote capturing</p>
    </li>
    </ul>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_removed_features_and_support">Removed Features and Support</h3>
    <div class="ulist">
    <ul>
    <li>
    <p>The CMake options starting with DISABLE_something were renamed ENABLE_something for consistency.
    For example DISABLE_WERROR=On became ENABLE_WERROR=Off.
    The default values are unchanged.</p>
    </li>
    </ul>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_protocol_support">New Protocol Support</h3>
    <div class="paragraph">
    <p>Allied Telesis Loop Detection (AT LDF), AUTOSAR I-PDU Multiplexer (AUTOSAR I-PduM), DTN Bundle Protocol Security (BPSec), DTN Bundle Protocol Version 7 (BPv7), DTN TCP Convergence Layer Protocol (TCPCL), DVB Selection Information Table (DVB SIT), Enhanced Cash Trading Interface 10.0 (XTI), Enhanced Order Book Interface 10.0 (EOBI), Enhanced Trading Interface 10.0 (ETI), FiveCo’s Legacy Register Access Protocol (5co-legacy), Generic Data Transfer Protocol (GDT), gRPC Web (gRPC-Web), Host IP Configuration Protocol (HICP), Huawei GRE bonding (GREbond), Locamation Interface Module (IDENT, CALIBRATION, SAMPLES - IM1, SAMPLES - IM2R0), Mesh Connex (MCX), Microsoft Cluster Remote Control Protocol (RCP), Protected Extensible Authentication Protocol (PEAP), Realtek, REdis Serialization Protocol v2 (RESP), Roon Discovery (RoonDisco), Secure File Transfer Protocol (sftp), Secure Host IP Configuration Protocol (SHICP), SSH File Transfer Protocol (SFTP), USB Attached SCSI (UASP), and ZBOSS Network Coprocessor product (ZB NCP)</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_updated_protocol_support">Updated Protocol Support</h3>
    <div class="paragraph">
    <p>Too many protocols have been updated to list here.</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_and_updated_capture_file_support">New and Updated Capture File Support</h3>
    <div class="paragraph">
    <p></p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_major_api_changes">Major API Changes</h3>
    <div class="ulist">
    <ul>
    <li>
    <p>proto.h: The field display types "STR_ASCII" and "STR_UNICODE" have been removed.
    Use "BASE_NONE" instead.</p>
    </li>
    <li>
    <p>proto.h: The field display types for floats have been extended and refactored.
    The type BASE_FLOAT has been removed. Use BASE_NONE instead. New display
    types for floats are BASE_DEC, BASE_HEX, BASE_EXP and BASE_CUSTOM.</p>
    </li>
    </ul>
    </div>
    </div>
    </div>
    </div>