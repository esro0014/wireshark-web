---
sticky: 0
headline: "Wireshark 2.1.0 Development Release"
body: |
    <p>
    Wireshark 2.1.0 has been released.
    This is an experimental release intended to test features that will go into
    Wireshark 2.2.
    Installers for Windows, OS X, and source code
    are now available.
    </p>
    <p>The following features are new (or have been significantly
    updated) since version 2.0.0:</p>
    <div class="itemizedlist">
    <ul class="itemizedlist" style="list-style-type: disc;">
    <li class="listitem">You can now switch between between Capture and
    File Format dissection of the current capture file via the View
    menu in the Qt GUI.</li>
    <li class="listitem">You can now show selected packet bytes as
    ASCII, HTML, Image, ISO 8859-1, Raw, UTF-8, a C array, or
    YAML.</li>
    <li class="listitem">You can now use regular expressions in Find
    Packet and in the advanced preferences.</li>
    <li class="listitem">Name resolution for packet capture now
    supports asynchronous DNS lookups only. Therefore the "concurrent
    DNS resolution" preference has been deprecated and is a no-op. To
    enable DNS name resolution some build dependencies must be present
    (currently c-ares). If that is not the case DNS name resolution
    will be disabled (but other name resolution mechanisms, such as
    host files, are still available).</li>
    <li class="listitem">The byte under the mouse in the Packet Bytes
    pane is now highlighted.</li>
    <li class="listitem">TShark supports exporting PDUs via the
    <code class="literal">-U</code> flag.</li>
    <li class="listitem">The Windows and OS X installers now come with
    the "sshdump" and "ciscodump" extcap interfaces.</li>
    <li class="listitem">Most dialogs in the Qt UI now save their size
    and positions.</li>
    <li class="listitem">The Follow Stream dialog now supports
    UTF-16.</li>
    <li class="listitem">The Firewall ACL Rules dialog has
    returned.</li>
    <li class="listitem">The Flow (Sequence) Analysis dialog has been
    improved.</li>
    </ul>
    </div>
    Official releases are available right now from the
    <a href="../download.html">download page</a>.
    </p>
