<!-- ------------------------------------ -->
<!-- Section 0: Configuration adjustments -->
<!-- ------------------------------------ -->
<!-- From http://www.mail-archive.com/mhonarc-users@mhonarc.org/msg01331.html -->

<nothread>

<idxfname>
.rss.ini
</idxfname>

<!-- ----------------------------- -->
<!-- Section 1: Specify RSS markup -->
<!-- ----------------------------- -->

<ssmarkup>
</ssmarkup>
<!--	Specify date sorting.  "sort" is a WML function, so we escape it
        out with an asterisk.
  -->
<*Sort>

<idxpgbegin>
[channel]
title: $WS_CAPNAME$ mailing list
link: https://www.wireshark.org/lists/
description: Email archive for $WS_CAPNAME$.
language: en-us

[image]
title: Wireshark
url: https://www.wireshark.org/image/wsbadge30.png
link: https://www.wireshark.org/lists/
width: 88
height: 30
</idxpgbegin>

<litemplate>
[msg.$WS_YYYYMM$.$MSGNUM$]
title: $SUBJECTNA:100$
pubDate: $MSGGMTDATE(CUR;%a, %d %h %Y %H:%M:%S GMT)$
link: https://www.wireshark.org/lists/$WS_LISTNAME$/$WS_YYYYMM$/$MSG$
guid: https://www.wireshark.org/lists/$WS_LISTNAME$/$WS_YYYYMM$/$MSG$
</litemplate>

<idxpgend>
</idxpgend>

<!-- ----------------------------------------------------- -->
<!-- Section 2: Remove any HTML left from default settings -->
<!-- ----------------------------------------------------- -->

<nostrip>
<listbegin>

</listbegin>

<listend>

</listend>
</nostrip>

<nodoc>
<nomsgpgs>
<nomultipg>
<main>

<!-- --- -->
<!-- End -->
<!-- --- -->
