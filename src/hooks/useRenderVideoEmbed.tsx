type RenderVideoEmbed = (
  videoUrl: string,
  width?: string,
  height?: string
) => {
  renderVideoEmbed: HTMLDivElement;
};

const useRenderVideoEmbed: RenderVideoEmbed = (
  videoUrl,
  width = "100%",
  height = "339px"
) => {
  const renderVideoEmbed = (
    <div>
      <iframe
        src={videoUrl}
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        width={width}
        height={height}
        frameBorder="0"
      ></iframe>
    </div>
  );

  return { renderVideoEmbed };
};

export default useRenderVideoEmbed;
