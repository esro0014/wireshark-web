import Accordion from "../Accordion/Accordion";
import styles from "./Download.module.scss";
import versions from "./Versions.json";
import { THEME_OPTIONS } from '../../utils/types';

const APPLE_LOGO_PATH = "/assets/icons/phosphor/apple-logo.svg";
const WINDOWS_LOGO_PATH = "/assets/icons/phosphor/windows-logo.svg";
const CODE_ICON_PATH = "/assets/icons/phosphor/code.svg";

// This uses the default prefix set in Versions.json.
// Pages using this component should have
//   <script is:inline src="/mirrors.js"/>
//   <script is:inline src="/js/update_mirror_url.js"></script>
//   <script type="module">update_mirror_url();</script>
// at the top of the page. /mirrors.js is generated every two minutes on the server.
// update_mirror_url() chooses a weighted random location set in mirrors.js and
// updates the download accordion URLs.

const DownloadAccordion: preact.FunctionComponent = () => {
  const download_prefix = versions.download_prefix;

  const accordionContent = {
    stableRelease: [
      {
        title: "Windows x64 Installer",
        link: download_prefix + "/win64/Wireshark-" + versions.stable + "-x64.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "Windows Arm64 Installer",
        link: download_prefix + "/win64/Wireshark-" + versions.stable + "-arm64.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "Windows x64 PortableApps®",
        link: download_prefix + "/win64/WiresharkPortable64_" + versions.stable + ".paf.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "macOS Arm Disk Image",
        link: download_prefix + "/osx/Wireshark%20" + versions.stable + "%20Arm%2064.dmg",
        icon: APPLE_LOGO_PATH,
      },
      {
        title: "macOS Intel Disk Image",
        link: download_prefix + "/osx/Wireshark%20" + versions.stable + "%20Intel%2064.dmg",
        icon: APPLE_LOGO_PATH,
      },
      {
        title: "Source Code",
        link: download_prefix + "/src/wireshark-" + versions.stable + ".tar.xz",
        icon: CODE_ICON_PATH,
      },
    ],
    oldStableRelease: [
      {
        title: "Windows x64 Installer",
        link:
          download_prefix + "/win64/Wireshark-win64-" + versions.oldstable + ".exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "Windows x64 PortableApps®",
        link:
          download_prefix + "/win64/WiresharkPortable64_" + versions.oldstable + ".paf.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "macOS Arm Disk Image",
        link:
          download_prefix + "/osx/Wireshark%20" + versions.oldstable + "%20Arm%2064.dmg",
        icon: APPLE_LOGO_PATH,
      },
      {
        title: "macOS Intel Disk Image",
        link:
          download_prefix + "/osx/Wireshark%20" + versions.oldstable + "%20Intel%2064.dmg",
        icon: APPLE_LOGO_PATH,
      },
      {
        title: "Source Code",
        link:
          download_prefix + "/src/wireshark-" + versions.oldstable + ".tar.xz",
        icon: CODE_ICON_PATH,
      },
    ],
    oldOldStableRelease: [
      {
        title: "Windows x64 Installer",
        link:
          download_prefix + "/win64/Wireshark-win64-" + versions.oldoldstable + ".exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "Windows x64 PortableApps®",
        link:
          download_prefix + "/win64/WiresharkPortable64_" + versions.oldoldstable + ".paf.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "Windows x86 Installer",
        link:
          download_prefix + "/win32/Wireshark-win32-" + versions.oldoldstable + ".exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "Windows x86 PortableApps®",
        link:
          download_prefix + "/win32/WiresharkPortable32_" + versions.oldoldstable + ".paf.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "macOS Arm Disk Image",
        link:
          download_prefix + "/osx/Wireshark%20" + versions.oldoldstable + "%20Arm%2064.dmg",
        icon: APPLE_LOGO_PATH,
      },
      {
        title: "macOS Intel Disk Image",
        link:
          download_prefix + "/osx/Wireshark%20" + versions.oldoldstable + "%20Intel%2064.dmg",
        icon: APPLE_LOGO_PATH,
      },
      {
        title: "Source Code",
        link:
          download_prefix + "/src/wireshark-" + versions.oldoldstable + ".tar.xz",
        icon: CODE_ICON_PATH,
      },
    ],
    developmentRelease: [
      {
        title: "Windows x64 Installer",
        link:
          download_prefix +
          "/win64/Wireshark-" +
          versions.development +
          "-x64.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "Windows Arm64 Installer",
        link:
          download_prefix +
          "/win64/Wireshark-" +
          versions.development +
          "-arm64.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "Windows x64 PortableApps®",
        link:
          download_prefix +
          "/win64/WiresharkPortable64_" +
          versions.development +
          ".paf.exe",
        icon: WINDOWS_LOGO_PATH,
      },
      {
        title: "macOS Arm Disk Image",
        link:
          download_prefix +
          "/osx/Wireshark%20" +
          versions.development +
          "%20Arm%2064.dmg",
        icon: APPLE_LOGO_PATH,
      },
      {
        title: "macOS Intel Disk Image",
        link:
          download_prefix +
          "/osx/Wireshark%20" +
          versions.development +
          "%20Intel%2064.dmg",
        icon: APPLE_LOGO_PATH,
      },
      {
        title: "Source Code",
        link:
          download_prefix +
          "/src/wireshark-" +
          versions.development +
          ".tar.xz",
        icon: CODE_ICON_PATH,
      },
    ],
    documentation: [
      {
        title: "Online (Multiple Pages)",
        link: "/docs/wsug_html_chunked/",
      },
      {
        title: "Online (Single Page)",
        link: "/docs/wsug_html/",
      },
      {
        title: "All Documentation",
        link: "/docs/",
      },
    ],
  };

  return (
    <div id="download-accordion" className={styles.downloadAccordion}>
      <Accordion
        title={"Stable Release: " + versions.stable}
        content={accordionContent.stableRelease}
        variant="bordered"
        open={true}
      />
      <Accordion
        title={"Old Stable Release: " + versions.oldstable}
        content={accordionContent.oldStableRelease}
        variant="bordered"
      />
      {versions.oldoldstable == null ? (
        ""
      ) : (
        <Accordion
          title={"Older Stable Release: " + versions.oldoldstable}
          content={accordionContent.oldOldStableRelease}
          variant="bordered"
        />
      )}
      {versions.development == null || versions.development == versions.stable ? (
        ""
      ) : (
        <Accordion
          title={"Development Release: " + versions.development}
          content={accordionContent.developmentRelease}
          variant="bordered"
        />
      )}
      <Accordion
        title="Documentation"
        content={accordionContent.documentation}
        variant="bordered"
      />
    </div>
  );
};

export default DownloadAccordion;
