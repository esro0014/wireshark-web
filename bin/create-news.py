#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :
'''\
Usage:

create-news.py [-f format] [article|range]

  Where "type" is one of
    ain : inline article
    apg : article page
    hld : headline linked to article, plus date
    rsi : RSS 2.0 <item/>
    mnu : Main menu li

    article specification is a file name (sans path)
    range specification is a python array range

  Examples
    make-news.py -f hld 0
    make-news.py -f ain 1:3
    make-news.py -f apg 20060607.py
    make-news.py -f hld 20060621b.py
'''

#
# Imports
#

import codecs
import sys
import glob
import json
import os
import os.path
import re
from types import *
import time
import datetime
import yaml
from string import Template

#
# Global variables
#

# months = {
#     '01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr',
#     '05': 'May', '06': 'Jun', '07': 'Jul', '08': 'Aug',
#     '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec'
#     }

art_re = re.compile(r'([12][90]\d\d)(\d\d)(\d\d)[a-z]?.yml')

news_trunc_width = 45

#
# Functions
#

def exit_err(msg=None):
    if msg: print(msg, file=sys.stderr)
    print(__doc__, file=sys.stderr)
    sys.exit(1)


def main():
    top_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
    astro_news_path = os.path.join(top_dir, 'src', 'components', 'News')
    astro_article_path = os.path.join(top_dir, 'src', 'pages', 'news')

    yarticles = glob.glob(os.path.join(astro_article_path, '_src', '20*.yml'))
    yarticles.sort()
    yarticles.reverse()

    # Asciidoctor uses h2 elements.
    body_replacements = {
        '<h2>': '<h3>',
        '<h2 ': '<h3 ',
        '</h2>': '</h3>',
    }

    with codecs.open(os.path.join(astro_article_path, '_src', 'news-article-template.astro'), mode='r', encoding='utf-8') as a_tmpl_f:
        a_tmpl_data = a_tmpl_f.read().encode('ascii', 'xmlcharrefreplace').decode('utf-8')

    # Write out our data

    articles = []

    print("Recent news:")
    for article_num, yart in enumerate(yarticles):
        res = art_re.search(yart)
        if res:
            year = int(res.group(1))
            month = int(res.group(2))
            day = int(res.group(3))

            with codecs.open(yart, mode='r', encoding='utf-8') as yart_f:
                yart_data = yart_f.read().encode('ascii', 'xmlcharrefreplace')
            yart_basename = os.path.basename(yart)
            article = yaml.safe_load(yart_data)
            article['yaml_source'] = os.path.join('_src', yart_basename)
            date = datetime.datetime(year, month, day)
            article['timestamp'] = time.mktime(date.timetuple())
            article['date'] = date.strftime('%B %e, %Y')
            article['filename'] = os.path.splitext(yart_basename)[0] + '.html'
            article['headline_escaped'] = article['headline'].replace("'", r"\'")
            for from_str, to_str in body_replacements.items():
                article['body'] = article['body'].replace(from_str, to_str)

            # Astro article file
            astro_filename = os.path.splitext(yart_basename)[0] + '.astro'
            with open(os.path.join(astro_article_path, astro_filename), 'w') as a_art_f:
                a_art_f.write(Template(a_tmpl_data).safe_substitute(article))

            # We need the first few article bodies for the "What's New" section in the main news page.
            if article_num < 10:
                print(f"  {year}-{month:02d}-{day:02d} {article['headline']}")
                article['body_escaped'] = "'" + article['body'].replace("'", r"\'") + "'"
            else:
                article['body_escaped'] = "none"
                del(article['body'])

            articles.append(article)

    with open(os.path.join(astro_news_path, 'Articles.json'), 'w') as astro_news_list_f:
        json.dump(articles, astro_news_list_f, indent=2)

#
# On with the show
#
if __name__ == '__main__':
    profile = 0
    if profile:
        profile.run('main()')
    else:
        sys.exit(main())
