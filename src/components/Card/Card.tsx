import styles from "./Card.module.scss";
import Modal from "../Modal/Modal";
import { ModalData } from "../Modal/types";
import useRenderVideoEmbed from "../../hooks/useRenderVideoEmbed";

export interface CardProps {
  id?: string;
  title: string;
  subtitle: string;
  imgUrl: string;
  link: string;
  videoUrl: string;
  shouldLinkToModal?: boolean;
  modalData: ModalData[];
}

const Card: preact.FunctionComponent<CardProps> = ({
  id,
  title,
  subtitle,
  imgUrl,
  link,
  videoUrl,
  shouldLinkToModal,
  modalData,
}) => {
  const { renderVideoEmbed } = useRenderVideoEmbed(videoUrl);

  return (
    <div id={id} className={styles.cardWrapper}>
      <a href={shouldLinkToModal ? `#modal-${title}` : link}>
        <div className={styles.imgSection}>
          {imgUrl ? <img src={imgUrl} /> : renderVideoEmbed}
        </div>
        <div className={styles.cardContent}>
          <h2>{title}</h2>
          <p>{subtitle}</p>
        </div>
        {shouldLinkToModal && (
          <Modal
            title={title}
            subtitle={subtitle}
            link={link}
            modalData={modalData}
          />
        )}
      </a>
    </div>
  );
};

export default Card;
