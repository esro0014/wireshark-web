#!/usr/bin/env python3
'''\
gen-appcasts.py - Generate signed Sparkle Appcasts for the latest release.
'''

#
# Imports
#

import argparse
# import hashlib
import importlib
import json
import os.path
# import re
# import subprocess
import sys
# import tempfile

from string import Template

#
# Globals
#

dl_pfx = 'https://www.wireshark.org/download'
version_info = importlib.import_module("get-version-info").version_info
# https://github.com/sparkle-project/Sparkle/blob/2.x/Resources/SampleAppcast.xml
appcast_tmpl = Template('''\
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:sparkle="http://www.andymatuschak.org/xml-namespaces/sparkle">
<channel>
  <title>Wireshark ${channel_title} Release</title>
  <link>${link}</link>
  <description>The latest ${channel} release of Wireshark.</description>
  <language>en</language>
${items}\
</channel>
</rss>
''')
macos_item_tmpl = Template('''\
  <item>
    <title>Version ${version}</title>
    <sparkle:minimumSystemVersion>${min_system_version}</sparkle:minimumSystemVersion>
    <pubDate>${pub_date}</pubDate>
    <sparkle:releaseNotesLink>https://www.wireshark.org/update/relnotes/wireshark-${version}.html</sparkle:releaseNotesLink>
    <enclosure
      url="${download_prefix}/${download_path}"
      sparkle:version="${version}" sparkle:shortVersionString="${version}"
      ${sparkle_signature}
      type="application/octet-stream"/>
      <!-- SHA256: ${installer_sha256} -->
  </item>
''')
windows_item_tmpl = Template('''\
  <item>
    <title>Version ${version}</title>
    <sparkle:releaseNotesLink>https://www.wireshark.org/update/relnotes/wireshark-${version}.html</sparkle:releaseNotesLink>
    <pubDate>${pub_date}</pubDate>
    <sparkle:minimumSystemVersion>${min_system_version}</sparkle:minimumSystemVersion>
    <enclosure
      url="${download_prefix}/${download_path}"
      sparkle:version="${version}" sparkle:shortVersionString="${version}"
      length="${bytes}"
      type="application/octet-stream">
      <!-- SHA256: ${installer_sha256} -->
    </enclosure>
  </item>
''')
architectures = (
    'wireshark-macos-arm64',
    'wireshark-macos-x86-64',
    'wireshark-windows-arm64',
    'wireshark-windows-x86-64',
    'wireshark-windows-x86',
)


#
# Classes
#

#
# Functions
#

def main():
    channel = 'stable'
    top_level = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
    component_path = os.path.join(top_level, 'src', 'components', 'Download')

    parser = argparse.ArgumentParser(description='Generate Sparkle signatures.')
    parser.add_argument('-d', '--development', action='store_true', help='Sign the development version.')
    args = parser.parse_args()

    if args.development:
        channel = 'development'

    dmg_version = version_info[channel]

    with open(os.path.join(component_path, 'Versions.json'), 'r') as key_f:
        versions = json.load(key_f)

    # XXX We should update these automatically.
    versions['wireshark-macos-arm64']['development']['min_system_version'] = '11.0.0' # Qt 6.5.3
    versions['wireshark-macos-arm64']['stable']['min_system_version'] = '11.0.0' # Qt 6.2.4
    versions['wireshark-macos-arm64']['oldstable']['min_system_version'] = '11.0.0' # Qt 6.2.4
    versions['wireshark-macos-arm64']['oldoldstable']['min_system_version'] = '11.0.0' # Qt 5.15.3

    versions['wireshark-macos-x86-64']['development']['min_system_version'] = '11.0.0' # Qt 6.5.3
    versions['wireshark-macos-x86-64']['stable']['min_system_version'] = '10.14.0' # Qt 6.2.4
    versions['wireshark-macos-x86-64']['oldstable']['min_system_version'] = '10.14.0' # Qt 6.2.4
    versions['wireshark-macos-x86-64']['oldoldstable']['min_system_version'] = '10.13.0' # Qt 5.15.3

    versions['wireshark-windows-arm64']['development']['min_system_version'] = '10.0.0' # Qt 6.5.3
    versions['wireshark-windows-arm64']['stable']['min_system_version'] = '10.0.0' # Qt 6.5.3

    versions['wireshark-windows-x86-64']['development']['min_system_version'] = '10.0.0' # Qt 6.5.3
    versions['wireshark-windows-x86-64']['stable']['min_system_version'] = '10.0.0' # Qt 6.5.3
    versions['wireshark-windows-x86-64']['oldstable']['min_system_version'] = '6.1.0' # Qt 5.15.2
    versions['wireshark-windows-x86-64']['oldoldstable']['min_system_version'] = '6.1.0' # Qt 5.15.2

    versions['wireshark-windows-x86']['oldoldstable']['min_system_version'] = '6.1.0' # Qt 5.15.2

    appcast_init = {
        'stable': {
            'channel': 'stable',
            'channel_title': 'Stable',
            'link': 'https://www.wireshark.org/download.html',
        },
        'development': {
            'channel': 'development',
            'channel_title': 'Development',
            'link': 'https://www.wireshark.org/development.html',
        },
    }

    # This determines which <item> tags are listed in each Appcast
    # XXX We should fill this in automatically.
    channel_items = {
        'stable': {
            'wireshark-macos-arm64': ['stable'],
            'wireshark-macos-x86-64': ['stable', 'oldoldstable'],
            'wireshark-windows-arm64': ['stable'],
            'wireshark-windows-x86-64': ['stable', 'oldstable'],
            'wireshark-windows-x86': ['oldoldstable'],
        }
    }

    if versions['development'] is None or versions['development'] == versions['stable']:
        channel_items['development'] = channel_items['stable']
    else:
        channel_items['development'] = {
            'wireshark-macos-arm64': ['development'],
            'wireshark-macos-x86-64': ['development', 'stable', 'oldstable', 'oldoldstable'],
            'wireshark-windows-arm64': ['development'],
            'wireshark-windows-x86-64': ['development'],
            'wireshark-windows-x86': ['oldoldstable'],
        }

    for channel in channel_items:
        print(f'Generating {channel} Appcasts')
        for arch in channel_items[channel]:
            item_tmpl = macos_item_tmpl
            if 'windows' in arch:
                item_tmpl = windows_item_tmpl

            appcast_info = appcast_init[channel].copy()
            appcast_info['items'] = ''
            installer_versions = []
            for item_version in channel_items[channel][arch]:
                item_info = appcast_init[channel].copy()
                item_info.update(versions[arch][item_version])
                item_info.update({
                    'download_prefix': versions['download_prefix'],
                    'version': versions[item_version],
                })
                installer_versions.append(f'w {versions[item_version]} sys {item_info["min_system_version"]}')

                try:
                    appcast_info['items'] += item_tmpl.substitute(item_info)
                except KeyError as e:
                    sys.stderr.write(f'Missing key {e.args[0]} in {channel} {arch} {item_version}:\n{repr(appcast_info)}')
                    sys.exit(1)

            print(f'  {arch} ({", ".join(installer_versions)})')
            with open(os.path.join(top_level, 'update', 'staging', arch, f'{channel}.xml'), 'w') as appcast_f:
                try:
                    appcast_f.write(appcast_tmpl.substitute(appcast_info))
                except KeyError as e:
                    sys.stderr.write(f'Missing key {e.args[0]} in:\n{repr(appcast_info)}')
                    sys.exit(1)

#
# On with the show
#

if __name__ == "__main__":
    sys.exit(main())
