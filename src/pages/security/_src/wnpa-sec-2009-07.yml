file_date: October 26, 2009
start_version: 0.10.10
end_version: 1.2.2
fixed_versions: 1.2.3

name: |
    Multiple vulnerabilities in Wireshark

affected_versions: |
    {{ start_version }} up to and including
    {{ end_version }}

body: |
    <p>
    Wireshark {{ fixed_versions }} fixes the following vulnerabilities:
    </p>
    
    <ul>
    
    <li>
      The Paltalk dissector could crash on alignment-sensitive processors.
      <!-- Fixed in trunk: r29064 -->
      <!-- Fixed in trunk-1.2: r30657 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3689">Bug
      3689</a>)
      <br>
      Versions affected: 1.2.0 to 1.2.2
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-3549">CVE-2009-3549</ulink>
    
    <li>
      The DCERPC/NT dissector could crash.
      <!-- Fixed in trunk: r30208 -->
      <!-- Fixed in trunk-1.2: r30658 -->
      <br>
      Versions affected: 0.10.10 to 1.2.2
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-3550">CVE-2009-3550</ulink>
    
    <li>
      The SMB dissector could crash.
      <!-- Fixed in trunk: r30595 -->
      <!-- Fixed in trunk-1.2: r30673 -->
      <br>
      Versions affected: 1.2.0 to 1.2.2
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-3551">CVE-2009-3551</ulink>
    
    </ul>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash remotely or by convincing
    someone to read a malformed packet trace file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    </p>
    
    <p>
    If are running Wireshark {{ end_version }} or earlier (including Ethereal)
    and cannot upgrade, you can work around each of the problems listed above by
    doing the following:
    </p>
    
    <ul class="item-list">
    
      <li>Disable the affected dissectors:
    
        <ul>
    
          <li>Select <em>Analyze&#8594;Enabled Protocols...</em> from the menu.
    
          <li>Make sure "Paltalk", "DCERPC", and "SMB" are all un-checked.
    
          <li>Click "Save", then click "OK".
    
        </ul>
    
    </ul>
    
    
    
