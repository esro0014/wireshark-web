# Main Wireshark Web Site

This repository contains the following:

- `README.md`: This file.

- `src`: Astro.js sources.

- `public`: Static site assets.

- `bin`: Support scripts.

- `data`: Data files.

- `update`: Appcasts and release notes for automatic updates.

- `site`: Old site assets built using Mynt.

- `.gitlab-ci.yml`: GitLab CI.

- `astro.config.mjs`, `package.json`, `tsconfig.json`, `tslint.json`, `yarn.lock`: Various configuration and metadata files.

## Building the Site

Run `yarn` to install necessary packages.

For local testing run

    yarn dev

To test a site build locally, run

    bin/build-dist.sh

This runs `yarn run astro build` and works around a [bug in Astro.js](https://github.com/withastro/astro/issues/6167).

You can serve and test your local build by running

    python3 -m http.server --directory dist

## Style Guildelines

Assets (fonts, images, etc) should be stored in `assets`.
This makes cache configuration easier on the server side.

Same-site links should be absolute (e.g. `/assets/some/file`, `/docs/`) and not relative.

Our official font is Lato.

Try to be as static as possible.
People like fast web sites.

Please don't use `<a target="_blank" ...`.
We can assume that our users know how to open links in new tabs, so we shouldn't force that decision.

Table headers should be left-aligned.

Should we remove top-level bullet points?
As Edward Tufte said, "Sentences are smarter than the grunts of bullet points."
I (Gerald) tend to agree.

## Caveats

Index files in subdirectories need to be named `default.html` instead of `index.html` when building.
See `astro.config.mjs` and `bin/build-dist.sh` for details.

## History

The Wireshark web site (and the Ethereal site before that) has used templating almost from its inception.
It orignally used [GNU m4](https://en.wikipedia.org/wiki/M4_(computer_language)), then the [Website Meta Language](https://thewml.github.io/), then [Mynt](https://github.com/uhnomoli/mynt).