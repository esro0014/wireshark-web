---
sticky: 0
headline: "Wireshark 1.5.0 Development Release"
body: |
    <p>
    Wireshark 1.5.0 has been released. Installers for Windows, OS X, and source code
    are now available. 
    </p>
    
        <div class="section" title="New and Updated Features">
          <div class="titlepage">
            <div>
              <div>
                <h3 class="title"><a id="NewFeatures"></a>New and Updated Features</h3>
              </div>
            </div>
          </div>
            <p>
          The following features are new (or have been significantly updated)
          since version 1.4:
            </p>
          <div class="itemizedlist"><ul class="itemizedlist" type="disc"><li class="listitem"><p>
                Wireshark can import text dumps, similar to text2pcap.
              </p></li><li class="listitem"><p>
                You can now view Wireshark's dissector tables (for example the
                TCP port to dissector mappings) from the main window.
              </p></li><li class="listitem"><p>
               TShark can show a specific occurrence of a field when using '-T fields'.
              </p></li><li class="listitem"><p>
                Custom columns can show a specific occurrence of a field.
              </p></li><li class="listitem"><p>
                You can hide columns in the packet list.
              </p></li><li class="listitem"><p>
                Wireshark can now export SMB objects.
              </p></li><li class="listitem"><p>
                dftest and randpkt now have manual pages.
              </p></li><li class="listitem"><p>
                TShark can now display iSCSI service response times.
              </p></li><li class="listitem"><p>
                Dumpcap can now save files with a user-specified group id.
              </p></li><li class="listitem"><p>
                Syntax checking is done for capture filters.
              </p></li><li class="listitem"><p>
                You can display the compiled BPF code for capture filters in the
                Capture Options dialog.
              </p></li><li class="listitem"><p>
                You can now navigate backwards and forwards through TCP and UDP
                sessions using
                  <span class="keycap"><strong>Ctrl</strong></span>+<span class="keycap"><strong>,</strong></span>
                and
                  <span class="keycap"><strong>Ctrl</strong></span>+<span class="keycap"><strong>.</strong></span>
                .
              </p></li><li class="listitem"><p>
                Packet length is (finally) a default column.
              </p></li><li class="listitem"><p>
                TCP window size is now avaiable both scaled and unscaled. A TCP
                window scaling graph is available in the GUI.
              </p></li><li class="listitem"><p>
    	    802.1q VLAN tags are now shown by the Ethernet II dissector.
    	  </p></li><li class="listitem"><p>
    	    Various dissectors now display some UTF-16 strings as proper Unicode
    	    including the DCE/RPC and SMB dissectors.
    	  </p></li><li class="listitem"><p>
    	    The RTP player now has an option to show the time of day in the
    	    graph in addition to the seconds since beginning of capture.
    	  </p></li><li class="listitem"><p>
                The RTP player now shows why media interruptions occur.
              </p></li><li class="listitem"><p>
                Graphs now save as PNG images by default.
              </p></li></ul></div><p>
    
        </p>
        </div>
        <div class="section" title="New Protocol Support">
            <div class="titlepage">
                <div>
                    <div><h3 class="title"><a id="NewProtocols"></a>New Protocol Support</h3>
                    </div>
                </div>
            </div>
            <p>
    
    ADwin,
    ADwin-Config,
    Apache Etch,
    Aruba PAPI,
    Constrained Application Protocol (COAP),
    Digium TDMoE,
    Ether-S-I/O,
    FastCGI,
    Fibre Channel over InfiniBand (FCoIB),
    Gopher,
    Gigamon GMHDR,
    IDMP,
    Infiniband Socket Direct Protocol (SDP),
    JSON,
    LISP Data,
    MikroTik MAC-Telnet,
    Mongo Wire Protocol,
    Network Monitor 802.11 radio header,
    OPC UA ExtensionObjects,
    PPI-GEOLOCATION-GPS,
    ReLOAD,
    ReLOAD Framing,
    SAMETIME,
    SCoP,
    SGSAP,
    Tektronix Teklink,
    WAI authentication,
    Wi-Fi P2P (Wi-Fi Direct)
    
        </p>
        </div>
    
        <div class="section" title="New and Updated Capture File Support">
            <div class="titlepage">
                <div>
                    <div><h3 class="title"><a id="NewCapture"></a>New and Updated Capture File Support</h3></div>
                </div>
            </div>
            <p>
    
    Apple PacketLogger,
    Catapult DCT2000,
    Daintree SNA,
    Endace ERF,
    HP OpenVMS TCPTrace,
    IPFIX (the file format, not the protocol),
    Lucent/Ascend debug,
    Microsoft Network Monitor,
    Network Instruments,
    TamoSoft CommView
    
        </p>
        </div>
        
    
    <p>
    Official releases are available right now from the
    <a href="../download.html">download page</a>.
    </p>
