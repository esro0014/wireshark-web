#!/usr/bin/env python3
'''\
    create-filter-directory: Read the outputs of `tshark -G fields` from different versions
    of TShark and generate a set of YAML files which can then be used to create the display
    filter reference using site/docs/dfref/_src/create-filter-page.py.
'''

# Imports

import argparse
import collections
import glob
import re
import os
import os.path
import json
import locale
import pprint
import sys
import itertools

from string import Template
from subprocess import call

# Global variables

version_re = re.compile(r'dfilter-list-(\d+\.\d+\.\d+[^.]*)\.txt')

this_path = os.path.dirname(__file__)
dfref_path = os.path.join(this_path, '..', 'dist-dfref')

all_versions = []
version_enum = {}
protocols = {}
tmpl_data = {}
version_cache = {}
version_spans = {}

# Templates

PROTO_INDEX_DEFS = '''\
{%% set num_protocols = %(num_protocols)s %%}
{%% set num_fields = %(num_fields)s %%}
{%% set latest_version = '%(latest_version)s' %%}
'''

# ./run/tshark -G ftypes | sort | sed -e "s/^/    '/" -e $'s/\t/\':\t\'/' -e "s/$/',/"
DATA_TYPES = {
    'FT_ABSOLUTE_TIME':	'Date and time',
    'FT_AX25':	'AX.25 address',
    'FT_BOOLEAN':	'Boolean',
    'FT_BYTES':	'Byte sequence',
    'FT_CHAR':	'Character (8 bits)',
    'FT_DOUBLE':	'Floating point (double-precision)',
    'FT_ETHER':	'Ethernet or other MAC address',
    'FT_EUI64':	'EUI64 address',
    'FT_FCWWN':	'Fibre Channel WWN',
    'FT_FLOAT':	'Floating point (single-precision)',
    'FT_FRAMENUM':	'Frame number',
    'FT_GUID':	'Globally Unique Identifier',
    'FT_IEEE_11073_FLOAT':	'IEEE-11073 Floating point (32-bit)',
    'FT_IEEE_11073_SFLOAT':	'IEEE-11073 floating point (16-bit)',
    'FT_INT16':	'Signed integer (16 bits)',
    'FT_INT24':	'Signed integer (24 bits)',
    'FT_INT32':	'Signed integer (32 bits)',
    'FT_INT40':	'Signed integer (40 bits)',
    'FT_INT48':	'Signed integer (48 bits)',
    'FT_INT56':	'Signed integer (56 bits)',
    'FT_INT64':	'Signed integer (64 bits)',
    'FT_INT8':	'Signed integer (8 bits)',
    'FT_IPv4':	'IPv4 address',
    'FT_IPv6':	'IPv6 address',
    'FT_IPXNET':	'IPX network number',
    'FT_NONE':	'Label',
    'FT_OID':	'ASN.1 object identifier',
    'FT_PROTOCOL':	'Protocol',
    'FT_RELATIVE_TIME':	'Time offset',
    'FT_REL_OID':	'ASN.1 relative object identifier',
    'FT_STRING':	'Character string',
    'FT_STRINGZ':	'Character string',
    'FT_STRINGZPAD':	'Character string',
    'FT_STRINGZTRUNC':	'Character string',
    'FT_SYSTEM_ID':	'OSI System-ID',
    'FT_UINT16':	'Unsigned integer (16 bits)',
    'FT_UINT24':	'Unsigned integer (24 bits)',
    'FT_UINT32':	'Unsigned integer (32 bits)',
    'FT_UINT40':	'Unsigned integer (40 bits)',
    'FT_UINT48':	'Unsigned integer (48 bits)',
    'FT_UINT56':	'Unsigned integer (56 bits)',
    'FT_UINT64':	'Unsigned integer (64 bits)',
    'FT_UINT8':	'Unsigned integer (8 bits)',
    'FT_UINT_BYTES':	'Byte sequence',
    'FT_UINT_STRING':	'Character string',
    'FT_VINES':	'VINES address',

    # Removed prior to 3.6
    'FT_PCRE':	'Compiled Perl-Compatible Regular Expression (GRegex) object',

    # 1.[02] only
    'GUID': 'Globally Unique Identifier',
    'OID':  'ASN.1 object identifier',

    # 1.[246] only
    'FT_EBCDIC':    'EBCDIC character string',

}


#
# Functions
#

# http://www.codinghorror.com/blog/2007/12/sorting-for-humans-natural-sort-order.html
def sort_nicely(unsorted):
    """ Sort the given list in the way that humans expect.
    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    unsorted.sort( key=alphanum_key )

# http://stackoverflow.com/questions/4628333/converting-a-list-of-integers-into-range-in-python
def ranges(i):
    for _, b in itertools.groupby(enumerate(i), lambda x_y: x_y[1] - x_y[0]):
        b = list(b)
        yield b[0][1], b[-1][1]

def version_span(version_list):
    '''
    Given a sorted list of version strings, compare it to the global
    "all_versions" list and collapse any consecutive runs.

    Arguments:
      version_list: sorted list of version strings

    Returns:
      Collapsed version string, e.g. "0.9.1 to 0.9.12, 0.10.0 to 0.10.2"
    '''

    version_key = ''.join(version_list)

    if version_key in version_spans:
        return version_spans[version_key]

    # Attach numbers to our versions so groupby can tell if they're consecutive
    ver_seq = list( (version_enum[v]) for v in version_list if v in version_enum )

    span_list = []
    for span in list(ranges(ver_seq)):
        if span[0] == span[1]:
            span_list.append(all_versions[span[0]])
        else:
            span_list.append(all_versions[span[0]] + ' to ' + all_versions[span[1]])

    span_str = ', '.join(span_list)
    version_spans[version_key] = span_str
    return span_str


def data_type_descr(dtype):
    '''
    Given a data type, return its name
    '''
    if dtype in DATA_TYPES:
        return DATA_TYPES[dtype]

    sys.stderr.write('Unknown field type ' + dtype + '\n')
    return "Unknown"

#
# Classes
#

class Protocol:
    '''
    Protocol class. Instances are stored in the "protocols" dict.
    '''
    def __init__(self, descr, name, version):
        self.descr = descr
        self.name = name
        self.versions = collections.deque([version])
        self.fields = {}

    def update_p_version(self, version):
        if version != self.versions[0]:
            self.versions.appendleft(version)

    def add_field(self, fkey, descr, name, dtype, version):
        try:
            self.fields[fkey].update_f_version(version)
        except KeyError:
            self.fields[fkey] = Field(descr, name, dtype, version)

    def html_path(self):
        dp_dir = os.path.join(dfref_path, self.name[0].lower())
        if not os.path.isdir(dp_dir):
            os.mkdir(dp_dir)
        return os.path.join(dp_dir, self.name.lower() + '.html')

    def get_values(self, field=None):
        vals = {}
        if field is None:
            # Index page protocol list
            vals['description'] = self.descr
            vals['name'] = self.name
            vals['versions'] = version_span(self.versions)
            vals['field_count'] = len(self.fields)
        else:
            # Protocol page field list
            vals['description'] = self.fields[field].descr
            vals['name'] = self.fields[field].name
            vals['data_type'] = data_type_descr(self.fields[field].dtype)
            vals['versions'] = version_span(self.fields[field].versions)
        return vals

    def has_fields(self):
        return len(self.fields) > 0


class Field:
    '''
    Field class. Instances are stored in each protocol's "fields" dict.
    '''
    def __init__(self, descr, name, dtype, version):
        self.descr = descr
        self.name = name
        self.dtype = dtype
        self.versions = collections.deque([version])

    def update_f_version(self, version):
        if version != self.versions[0]:
            self.versions.appendleft(version)


class PctTemplate(Template):
    delimiter = '%'

#
# Main program
#

def main():
    top_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
    # this_file = os.path.basename(__file__)
    locale.setlocale(locale.LC_NUMERIC, 'en_US.UTF-8')

    parser = argparse.ArgumentParser(description='Display filter reference generator.')
    parser.add_argument('--html-only', action='store_true', help='Only update the reference HTML pages.')
    args = parser.parse_args()

    tmpl_data['num_protocols'] = 0
    tmpl_data['num_fields'] = 0
    tmpl_data['latest_version'] = '0.0.0'
    latest_ver_fields = 0
    latest_ver_protos = 0
    tr_tmpl = Template('    <tr id="${name}"><td>${wbr_name}</td><td>${description}</td><td>${data_type}</td><td>${versions}</td></tr>\n')

    # Parse through each display filter list
    df_files = glob.glob(os.path.join(top_dir, 'data', 'dfilter-db', 'dfilter-list-*.txt'))
    sort_nicely(df_files)

    astro_proto_tmpl = None
    if args.html_only:
        astro_proto_tmpl_path = os.path.normpath(os.path.join(top_dir, 'templates', 'dfref-protocol-page.html'))
        try:
            with open(astro_proto_tmpl_path, mode='r') as astro_proto_tmpl_f:
                astro_proto_tmpl = astro_proto_tmpl_f.read()
        except:
            sys.stderr.write(f'Unable to open {astro_proto_tmpl_path}. Did you build the site?\n')
            sys.exit(1)


    if not os.path.isdir(dfref_path):
        os.mkdir(dfref_path)

    # The first name, description, and data type wins, so process the most
    # recent versions first.
    for filename in reversed(df_files):
        res = version_re.match(os.path.basename(filename))
        if res is None:
            print(f'Unable to process {filename}')
            continue

        version = res.group(1)

        all_versions.insert(0, version)
        print('Added version ' + version)

        if tmpl_data['latest_version'] == '0.0.0':
            tmpl_data['latest_version'] = version
        latest_ver_fields = 0
        latest_ver_protos = 0
        line_num = 0

        with open(filename, mode='r', encoding='utf-8') as dump_f:
            for line in dump_f.readlines():
                line_num += 1
                line = line.strip()
                parts = line.split('\t')
                if parts[0] == 'P':
                    descr = parts[1]
                    name = parts[2]
                    proto_key = parts[2].lower()
                    try:
                        protocols[proto_key].update_p_version(version)
                    except KeyError:
                        protocols[proto_key] = Protocol(descr, name, version)
                    latest_ver_protos += 1
                else:	# We're assuming 'F'
                    if len(parts) < 5:
                        print('Error in %s:%d: %s' % (filename, line_num, line))
                        return 1
                    descr = parts[1]
                    name = parts[2]
                    fkey = name.lower()
                    dtype = parts[3]
                    proto_key = parts[4].lower()
                    protocols[proto_key].add_field(fkey, descr, name, dtype, version)
                    latest_ver_fields += 1

    tmpl_data['num_protocols'] = len(protocols)
    for proto_key in iter(protocols.keys()):
        tmpl_data['num_fields'] += len(protocols[proto_key].fields)

    version_enum.update( (v, k) for k, v in enumerate(all_versions) )

    proto_keys = list(protocols.keys())
    proto_keys.sort()

    astro_proto_list = []
    section_proto_list = []
    initial = ''
    # For html-only, break field names before each period and underscore.
    wbr_table = {k: f'<wbr>{k}' for k in '._'}
    wbr_trans = str.maketrans(wbr_table)
    for proto_key in proto_keys:
        if not protocols[proto_key].has_fields():
            continue

        if initial != proto_key[0].lower():
            initial = proto_key[0].lower()
            section_proto_list = []
            astro_proto_list.append({'name': initial, 'protocols': section_proto_list})

        # We want two copies: One with a 'fields' element for the individual page
        # and one without for the index page.
        proto_values = protocols[proto_key].get_values()
        section_proto_list.append(proto_values.copy())
        proto_values['fields'] = ''

        field_keys = list(protocols[proto_key].fields.keys())
        field_keys.sort()
        for field_key in field_keys:
            field_values = protocols[proto_key].get_values(field_key)
            field_values['wbr_name'] = field_values['name'].translate(wbr_trans)
            proto_values['fields'] += tr_tmpl.substitute(field_values)

        if args.html_only:
            with open(protocols[proto_key].html_path(), mode='w', encoding='utf-8') as proto_f:
                proto_f.write(PctTemplate(astro_proto_tmpl).substitute(proto_values))

    if not args.html_only:
        astro_proto_list_path = os.path.join(top_dir, 'src', 'components', 'DisplayFilterReference', 'ProtocolList.json')
        with open(astro_proto_list_path, mode='w') as astro_proto_list_f:
            json.dump(astro_proto_list, astro_proto_list_f, indent=2)

        astro_metadata_path = os.path.join(top_dir, 'src', 'components', 'DisplayFilterReference', 'Metadata.json')
        with open(astro_metadata_path, mode='w') as astro_metadata_f:
            json.dump(tmpl_data, astro_metadata_f, indent=2)

    return 0

#
# On with the show
#
if __name__ == '__main__':
    PROFILE = 0
    if PROFILE:
        import cProfile
        outname = os.path.splitext(os.path.basename(__file__))[0] + '.cprofile'
        cProfile.run('main()', filename=outname)
    else:
        sys.exit(main())
