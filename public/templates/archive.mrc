<!-- MHonArc Resource File -->
<!-- $Id$
     Originally taken from the "date.mrc" example from the MHonArc
     distribution, written by Earl Hood <earl @ earlhood . com>
  -->
<!-- This resource file utilizes the day grouping feature of MHonArc
     to format the main index.
  -->

<!-- Set some general options -->
<spammode>

<!-- Taken from /usr/share/doc/mhonarc/examples/utf-8-encode.mrc -->
<TextEncode>
utf-8; MHonArc::UTF8::to_utf8; MHonArc/UTF8.pm
</TextEncode>

<!-- With data translated to UTF-8, it simplifies CHARSETCONVERTERS -->
<CharsetConverters override>
default; mhonarc::htmlize
</CharsetConverters>

<!-- Need to also register UTF-8-aware text clipping function -->
<TextClipFunc>
MHonArc::UTF8::clip; MHonArc/UTF8.pm
</TextClipFunc>

<idxfname>
index.html
</idxfname>

<FieldOrder>
from
date
</FieldOrder>

<otherindexes>
rss.mrc
</otherindexes>

<!--	Specify date sorting.  "sort" is a WML function, so we escape it
        out with an asterisk.
  -->
<*Sort>

<!--	Set USELOCALTIME since local date formats are used when displaying
	dates.
  -->
<UseLocalTime>

<!--    Define message local date format to print day of the week, month,
	month day, and year.  Format used for day group heading.
  -->
<MsgLocalDateFmt>
%B %d, %Y
</MsgLocalDateFmt>

<!-- Begin main index page -->
<include>
idxpg.mrc.html
</include>

<!-- Thread index page -->
<include>
threadpg.mrc.html
</include>

<!-- Message page -->
<include>
msgpg.mrc.html
</include>
