const links = {
  download: {
      name: "Download",
      url: "/download.html"
  },
  foundation: {
    name: "Wireshark Foundation",
    url: "https://wiresharkfoundation.org/"
  },
  privacyPolicy: {
    name: "Privacy Policy",
    url: "https://wiresharkfoundation.org/privacy-policy/"
  },
  news: {
    name: "News",
    url: "/news/"
  },
  learn: {
    name: "Learn",
    url: "/learn"
  },
  sharkFest: {
      name: "SharkFest",
      url: "https://sharkfest.wireshark.org"
  },
  wiresharkNewsFacebook: {
    name: "WiresharkNews on Facebook",
    url: "https://www.facebook.com/WiresharkNews/"
  },
  wiresharkIOCExchange: {
    name: "WiresharkNews on the Fediverse",
    url: "https://ioc.exchange/@wireshark"
  },
  wiresharkNewsTwitter: {
    name: "WiresharkNews on Twitter",
    url: "https://twitter.com/WiresharkNews"
  },
  sharkFestYouTube: {
    name: "SharkFest YouTube",
    url: "https://www.youtube.com/c/SharkFestWiresharkDeveloperandUserConference"
  },
  shop: {
    name: "Shop",
    url: "https://wiresharkfoundation.org/shop/#!/"
  },
  members: {
    name: "Members",
    url: "/members.html"
  },
  donate: {
    name: "Donate",
    url: "https://wiresharkfoundation.org/donate/"
  },
  apac24: {
    name: "Apac 24",
    url: "/register/apac-24"
  },
  getAcquainted: {
      name: "Get Acquainted",
      subLinks: [
          {
              name: "About",
              url: "/about.html"
          },
          {
              name: "Download",
              url: "/download.html"
          },
          {
              name: "Blog",
              url: "https://blog.wireshark.org/"
          },
          {
              name: "Code of Conduct",
              url: "/code-of-conduct.html"
          }
      ]
  },
  getHelp: {
      name: "Get Help",
      subLinks: [
          {
              name: "Ask a Question",
              url: "https://ask.wireshark.org/"
          },
          {
              name: "FAQs",
              url: "/faq.html"
          },
          {
              name: "Documentation",
              url: "/docs/"
          },
          {
              name: "Mailing Lists",
              url: "/lists/"
          },
          {
              name: "Online Tools",
              url: "/tools/"
          },
          {
              name: "Issue Tracker",
              url: "https://gitlab.com/wireshark/wireshark/-/issues"
          },
          {
              name: "Wiki",
              url: "https://wiki.wireshark.org/"
          },
      ]
  },
  develop: {
      name: "Develop",
      subLinks: [
          {
              name: "Get Involved",
              url: "/develop.html"
          },
          {
              name: "Developer's Guide",
              url: "/docs/wsdg_html_chunked"
          },
          {
              name: "Browse the Code",
              url: "https://gitlab.com/wireshark/wireshark/-/tree/master"
          }
      ]
  },
}

export default links

export interface SponsorLink {
    url: string;
    name: string;
    imgPath: string;
    sponsorshipLevel?: string;
}

export const sponsorLinks = [
    {
        url: "https://sysdig.com",
        name: "Sysdig",
        imgPath: "/assets/img/sponsors/sysdig-white.png"
    },
    {
        url: "https://scos.nl/",
        name: "SCOS",
        imgPath: "/assets/img/sponsors/scos-white.png"
    },
    {
        url: "https://endace.com",
        name: "Endace",
        imgPath: "/assets/img/sponsors/endace_big.png",
        sponsorshipLevel: "platinum"
    },
    {
        url: "https://liveaction.com",
        name: "LiveAction",
        imgPath: "/assets/img/sponsors/liveaction.png",
        sponsorshipLevel: "platinum"
    },
    {
        url: "https://www.veeam.com/",
        name: "Veeam",
        imgPath: "/assets/img/sponsors/veeam-white.png",
        sponsorshipLevel: "silver"
    },
    {
        url: "https://www.fmad.io/",
        name: "fmadio",
        imgPath: "/assets/img/sponsors/fmadio.png"
    },
    {
        url: "https://allegro-packets.com/en/",
        name: "Allegro Packets",
        imgPath: "/assets/img/sponsors/allegro.png"
    },
    {
        url: "https://www.profitap.com/",
        name: "Profitap",
        imgPath: "/assets/img/sponsors/profitap-white.png"
    },
    {
        url: "https://ntop.org",
        name: "ntop",
        imgPath: "/assets/img/sponsors/ntop.png"
    },
    {
        url: "https://riverbed.com",
        name: "Riverbed",
        imgPath: "/assets/img/sponsors/riverbed-white.png"
    },
    {
        url: "https://www.hostingsolutions.it/en/",
        name: "Hosting Solutions",
        imgPath: "/assets/img/sponsors/hosting-solutions.png"
    },
    {
        url: "https://digitalocean.com",
        name: "DigitalOcean",
        imgPath: "/assets/img/sponsors/digital-ocean.png"
    },
    {
        url: "https://npcap.com/",
        name: "Npcap",
        imgPath: "/assets/img/sponsors/npcap.png",
        sponsorshipLevel: "silver"
    }
]