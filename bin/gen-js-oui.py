#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''\
gen-js-oui - Generates a list of OUIs formatted as a JSON array.
'''

#
# Imports
#

from datetime import datetime, timezone
import json
import os.path
import re
import sys
import urllib.request, urllib.parse, urllib.error

manuf_url = 'https://www.wireshark.org/download/automated/data/manuf'

# The manuf contains comments, empty lines or tab-separated fields:
# 1. Address prefix, exactly one of:
#   xx:xx:xx
#   xx:xx:xx:x0:00:00/28
#   xx:xx:xx:xx:x0:00/36
# 2. Short name (max 8 characters), for example "Xerox"
# 3. Optional long name, for example "Xerox Corporation".
# 4. Optional comment (part of the long name):
#   # blablabla
# See also parse_ether_address_fast in epan/addr_resolv.c
re_line = re.compile(r'''
    # 1. prefix
    (?P<prefix>[0-9a-f]{2}:[0-9a-f:]+)
    (?:/(?P<mask>28|36))?
    \s+
    # Skip the short name if the long name is present.
    (?:\S+ \s+)?
    (?P<name>[^\t#].*)
    $
''', re.I | re.X)

pfx_trans = str.maketrans('', '', ':-')
desc_trans = str.maketrans('', '', '"')

def main():
    min_entries = 1000
    entries = {}

    try:
        print('Fetching manuf.')
        req_headers = { 'User-Agent': 'Wireshark make-manuf' }
        req = urllib.request.Request(manuf_url, headers=req_headers)
        mf = urllib.request.urlopen(req)
        # Ideally this is the date when the manuf file is last modified, but
        # that might require a direct query to the GitLab API.
        now = datetime.now(timezone.utc).isoformat(timespec='seconds') + 'Z'
        comma = ''
        for line in mf:
            entry = {'comma': comma}
            line = line.strip()
            line = line.decode('utf-8')

            # Skip empty lines or comments.
            if not line or line.startswith('#'):
                continue

            m = re_line.match(line)
            if not m:
                print('Unrecognized manuf line:', line)
                #continue
                return 1

            prefix = m.group('prefix').replace(':', '').lower()
            mask = m.group('mask')
            if mask:
                prefix = prefix[:int(mask)//4]

            if prefix in entries:
                print('Warning: ignoring duplicate prefix', prefix)
                continue

            name = m.group('name').replace('\t', ' ')
            # While '<' could be escaped by '\u003c' to protect against XSS, the
            # only occurence ("G3 Technologies< Inc") is likely a typo for
            # comma, so just strip it.
            name = name.replace('<', '')
            entries[prefix] = name
    except IOError as err:
        print('Oops: {}'.format(err.getcode()))
        return 1

    entry_count = len(entries)
    if entry_count < min_entries:
        print(('Too few entries: %d' % entry_count))
        return 1

    manuf_json = os.path.normpath(os.path.join(os.path.dirname(__file__), '..', 'public', 'assets', 'json', 'manuf.json'))

    try:
        print('Writing %d entries to %s' % (entry_count, manuf_json))
        with open(manuf_json, 'w') as f:
            obj = {
                'created_at': now,
                'data': entries
            }
            json.dump(obj, f, separators=(',', ':'), sort_keys=True)
    except Exception as e:
        print("Can't write %s: %s" % (manuf_json, e))
        return 1

    return 0

if __name__ == "__main__":
    sys.exit(main())
