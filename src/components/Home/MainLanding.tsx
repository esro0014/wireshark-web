import { FunctionComponent } from "preact";
import useScrollToDownloads from "../../hooks/useScrollToDownloads";
import styles from "./Homepage.module.scss";
import links from "../../utils/links";

const MainLanding: FunctionComponent = () => {
  const { handleDownloadClick } = useScrollToDownloads();

  return (
    <section id={styles.mainLanding}>
      <div className={styles.mainWrapper}>
        <div className={styles.leftSide}>
          <a onClick={handleDownloadClick}>
            <img
              src="/assets/icons/wireshark-fin.png"
              srcset="/assets/icons/wireshark-fin@2x.png 2x"
              alt="Wireshark fin"
              width="30px"
              height="30px"
            />
            <h2>Download Wireshark Now</h2>
            <img
              src="/assets/icons/right-arrow-blue.png"
              alt="Right arrow"
              width="25px"
              height="14px"
              className={styles.rightArrow}
            />
          </a>
          <h1>The world's most popular network protocol analyzer</h1>
          <h3>
            Get started with Wireshark today and see why it is the standard
            across many commercial and non-profit enterprises.
          </h3>
          <div className={styles.buttonContainer}>
            <button onClick={handleDownloadClick} className="primary-button">
              Get started
            </button>
            <a target="_blank" rel="noreferrer" href={links.donate.url}><button className="primary-button">
              {links.donate.name}
            </button></a>
          </div>
        </div>
        <div className={styles.rightSide}>
          <iframe width="100%" height="100%" loading="lazy" src="https://www.youtube-nocookie.com/embed/3HdKhen0Gqw" srcDoc="<style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto;height:100%}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style><a href=https://www.youtube-nocookie.com/embed/3HdKhen0Gqw><img src=/assets/img/wireshark4-overview.webp alt='Wireshark 4.0 Overview Video'><span>▶</span></a>"title="Wireshark 4.0 Overview Video" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen />
        </div>
      </div>
    </section>
  );
};

export default MainLanding;
