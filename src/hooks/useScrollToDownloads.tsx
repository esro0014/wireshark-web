const useScrollToDownloads = () => {
  const handleDownloadClick = (): void => {
    window.scrollTo({
      top: 2000,
      left: 0,
      behavior: "smooth",
    });
  };

  return { handleDownloadClick };
};

export default useScrollToDownloads;
