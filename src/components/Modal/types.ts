interface LinksData {
  title: string;
  link: string;
}

export interface ModalData {
  name: string;
  link?: string;
  links?: LinksData[];
  linkEmbed?: string;
}
