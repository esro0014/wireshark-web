#!/bin/bash
#
# Convert-man-pages - Publish man pages in a Wireshark build directory.

set -e -u -o pipefail

BASEDIR="$( realpath "$( dirname "$0" )" )"
ASTRO_SITE_DIR="$BASEDIR/../public/docs/man-pages"
TOP_LEVEL=$( git rev-parse --show-toplevel )

cd "${TOP_LEVEL}/build" || exit 1

ninja docs || cmake --build . --target docs
cd doc

if [[ ! -s wireshark.1 ]] || [[ ! -s wireshark.html ]] || [[ ! -s tshark.1 ]] || [[ ! -s tshark.html ]] ; then
    echo "Can't find {wireshark,tshark}.{1,html} (or one of them is empty)"
    exit 1
fi

# Astro
ls *.html | \
while read -r MANPAGE ; do
    [[ $MANPAGE = release-notes.html ]] && continue
    [[ $MANPAGE = *2deb.html ]] && continue
    [[ $MANPAGE = *dftest.html ]] && continue
    echo "Converting $MANPAGE → $ASTRO_SITE_DIR/$MANPAGE"
    sed \
        -e 's:href=".*ws.css":href="/assets/css/ws.css":' \
        -e '/link rev="made" href="mailto:/d' \
        < "$MANPAGE" > "$ASTRO_SITE_DIR/$MANPAGE"
done

echo "Running create-man-page-list"
"$BASEDIR/create-man-page-list.py"
