#!/bin/bash
#

set -e -u -o pipefail

BINDIR=$( dirname "$0" )
ASTRO_SITE_DIR="$BINDIR/../src"
WSDEVDIR=~/Development/wireshark

if [ -d "$WSDEVDIR/build" ] ;then
    pushd .
    cd "$WSDEVDIR/build"
    ninja faq_html
    popd
    xmllint \
        --html \
        --format \
        --xpath '//body/*' \
        "$WSDEVDIR/build/doc/faq.html" \
        > "$ASTRO_SITE_DIR/components/FAQContents/FAQContents.html"
fi

"$BINDIR/get-version-info.py"
"$BINDIR/create-relnote-list.py"
"$BINDIR/get-authors.py"
"$BINDIR/gen-js-oui.py"
"$BINDIR/gen-appcasts.py"

"$BINDIR/create-advisories.py"
git add "$ASTRO_SITE_DIR/pages/security/wnpa-sec-[0-9][0-9][0-9][0-9]-[0-9][0-9]*.astro" 2> /dev/null

"$BINDIR/create-news.py"
git add "$ASTRO_SITE_DIR/src/pages/news/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]*.astro" 2> /dev/null
git add "$ASTRO_SITE_DIR/src/pages/news/default.astro" 2> /dev/null
git add "$ASTRO_SITE_DIR/_templates/news_list.html"

cp "$WSDEVDIR/docbook/ws.css" "$ASTRO_SITE_DIR/../public/css/ws.css"
git add "$ASTRO_SITE_DIR/../public/css/ws.css"
