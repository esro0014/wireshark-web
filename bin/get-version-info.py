#!/usr/bin/env python3
'''\
get-version-info - Generate configuration files for version numbers, dates, and sizes.
'''

#
# Imports
#

import configparser
import json
import os.path
import re
import subprocess
import sys
import traceback
import urllib.request, urllib.error, urllib.parse

from email.utils import parsedate_tz
from string import Template

#
# Global variables
#

top_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
url_pfx = 'https://www.wireshark.org/download/'
version_info = {
    'generator':   os.path.basename(__file__),
    'stable':           '4.2.5',
    'oldstable':        '4.0.15',
    'oldoldstable':     '3.6.24',
    # 'development':      None,
    'development':      '4.3.0rc1',
    # 'stable_log':       '0.0.0',
    # 'oldstable_log':    '0.0.0',
    # 'development_log':  '0.0.0',
    # The default prefix for each of the downloads below. fill_dl.js
    # will replace this with the nearest mirror.
    'download_prefix':  'https://2.na.dl.wireshark.org',
    'wireshark-macos-arm64': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'oldoldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
    'wireshark-macos-x86-64': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'oldoldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
    'wireshark-windows-arm64': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'oldoldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
    'wireshark-windows-x86': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'oldoldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
    'wireshark-windows-x86-64': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'oldoldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
}

update_relnote_tmpl = Template('''\
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="Asciidoctor 2.0.18">
<title>Wireshark ${version} Release Notes</title>
<link rel="stylesheet" href="./ws.css">
<!--[if IE]><base target="_blank"><![endif]-->
</head>
<body class="article">

${whats_new}

</body>
</html>
''')


#
# Functions
#

def get_installer_info(dl_dir, installer):
    download_path = f'{dl_dir}/{installer}'
    req = urllib.request.Request(f'{url_pfx}{download_path}')
    req.get_method = lambda : 'HEAD'
    installer_info = None
    try:
        resp = urllib.request.urlopen(req)
        headers = dict(resp.info())
        content_length = int(headers['Content-Length'])
        last_modified = headers['Last-Modified']
        print(f'Got {last_modified}, {content_length / 1024 / 1024:.0f} MiB for {urllib.parse.unquote(installer)}')
        installer_info = {
            'bytes': content_length,
            'download_path': download_path,
            'pub_date': last_modified,
            'installer_sha256': '<no sha256>', # Should be overwritten by get_signature_info().
        }
    except urllib.error.HTTPError as err:
        sys.stderr.write(f'\n= ERROR: Unable to download {url_pfx}{download_path}: {err.code}\n')

    if installer_info is None:
        sys.exit(1)

    return installer_info


def get_signature_info(version, os, arch):
    relinfo_ini = f'release-info-{version}-{os}-{arch}.ini'
    relinfo_url = f'{url_pfx}/automated/.release/{relinfo_ini}'
    with urllib.request.urlopen(relinfo_url) as response:
        relinfo = configparser.ConfigParser()
        relinfo.read_string(response.read().decode('utf-8'))

    try:
        sparkle_signature = relinfo['DEFAULT']['sparkle_signature']
        installer_sha256 = relinfo['DEFAULT']['dmg_sha256']
        print(f'Got {sparkle_signature}, {installer_sha256} for {relinfo_ini}')
    except KeyError:
        sparkle_signature = '<no signature>'
        installer_sha256 = relinfo['DEFAULT']['nsis_sha256']
        print(f'Got {installer_sha256} for {relinfo_ini}')

    return {
        'sparkle_signature': sparkle_signature,
        'installer_sha256': installer_sha256,
    }


def update_pad_xml():
    '''wireshark-pad.xml is used by our monitoring system to determine the current stable version number.'''
    lines = ''
    pub_time = parsedate_tz(version_info['wireshark-windows-x86-64']['stable']['pub_date'])
    pub_year = pub_time[0]
    pub_month = pub_time[1]
    pub_day = pub_time[2]
    size_bytes = version_info['wireshark-windows-x86-64']['stable']['bytes']
    download_file = os.path.split(version_info['wireshark-windows-x86-64']['stable']['download_path'])[1]
    substitutions = {
        'Program_Version': version_info['stable'],
        'Program_Release_Year': pub_year,
        'Program_Release_Month': pub_month,
        'Program_Release_Day': pub_day,
        'File_Size_Bytes': size_bytes,
        'File_Size_K': f'{size_bytes / 1024:.2f}',
        'File_Size_MB': f'{size_bytes / 1024 / 1024:.2f}',
        'Primary_Download_URL': 'https://www.wireshark.org/download/win64/all-versions/' + download_file,
        'Secondary_Download_URL': 'https://1.na.dl.wireshark.org/win64/all-versions/' + download_file,
        'Additional_Download_URL_1': 'https://2.na.dl.wireshark.org/win64/all-versions/' + download_file,
    }

    with open(os.path.join(top_dir, 'public', 'wireshark-pad.xml'), 'r') as pad_xml_f:
        for line in pad_xml_f:
            for tag, value in substitutions.items():
                pattern = rf'(?P<open>.*?<{tag}>).+?(?P<close></{tag}>.*)'
                repl = rf'\g<open>{value}\g<close>'
                line = re.sub(pattern, repl, line)
            lines += line

    with open(os.path.join(top_dir, 'public', 'wireshark-pad.xml'), 'w') as pad_xml_f:
        pad_xml_f.write(lines)
        print('\nUpdated wireshark-pad.xml')


def get_dfilter_list(version):
    dfilter_file = f'dfilter-list-{version}.txt'
    req = urllib.request.Request(f'{url_pfx}automated/.release/{dfilter_file}')
    resp = urllib.request.urlopen(req)
    dfilter_lines = resp.read().decode('utf-8')
    if len(dfilter_lines) < 1 * 1000 * 1000:
        sys.stderr.write(f'{dfilter_file} has length {len(dfilter_lines)}\n')
        sys.exit(1)

    with open(os.path.join(top_dir, 'data/dfilter-db', dfilter_file), 'w') as dfilter_f:
        dfilter_f.write(dfilter_lines)
        print(f'Fetched {dfilter_file}')


def convert_release_note(version):
    release_note_file = f'wireshark-{version}.html'
    req = urllib.request.Request(f'{url_pfx}automated/.release/release-notes-{version}.html')
    resp = urllib.request.urlopen(req)
    release_note_lines = resp.read().decode('utf-8')

    if len(release_note_lines) < 1 * 1000:
        sys.stderr.write(f'{release_note_lines} has length {len(release_note_lines)}\n')
        sys.exit(1)

    wn_proc = subprocess.run(
        [
            'xmllint',
            '--html',
            '--format',
            '--xpath', '/html/body[@class="article"]/div[@id="content"]/div[@class="sect1"]/h2[@id="_whats_new"]/..',
            '-'
        ],
        encoding='utf-8',
        input=release_note_lines,
        capture_output=True
    )
    if wn_proc.returncode:
        sys.stderr.write(wn_proc.stderr)
        return wn_proc.returncode

    whats_new = re.sub(r's/ *target="_top"', '', wn_proc.stdout).strip()
    with open(os.path.join(top_dir, 'update', 'relnotes', release_note_file), 'w') as relnote_f:
        relnote_f.write(update_relnote_tmpl.substitute({'version': version, 'whats_new': whats_new}))
        print(f'Generated update/relnotes/{release_note_file}')

    article_proc = subprocess.run(
        [
            'xmllint',
            '--html',
            '--format',
            '--xpath', '/html/body[@class="article"]/*',
            '-'
        ],
        encoding='utf-8',
        input=release_note_lines,
        capture_output=True
    )
    if article_proc.returncode:
        sys.stderr.write(article_proc.stderr)
        return article_proc.returncode

    article = re.sub(r's/ *target="_top"', '', article_proc.stdout)
    with open(os.path.join(top_dir, 'src', 'pages', 'docs', 'relnotes', '_src', release_note_file), 'w') as relnote_f:
        relnote_f.write(article)
        print(f'Generated src/pages/docs/relnotes/_src/{release_note_file}')

# The server returns GMT dates. We might want to add a local-fudged time value.
def main():
    if version_info['development'] is None:
        version_info['development'] = version_info['stable']

    try:
        installer = f"Wireshark-{version_info['stable']}-x64.exe"
        version_info['wireshark-windows-x86-64']['stable'].update(get_installer_info('win64', installer))
        version_info['wireshark-windows-x86-64']['stable'].update(get_signature_info(version_info['stable'], 'windows', 'x64'))

        installer = f"Wireshark-{version_info['stable']}-arm64.exe"
        version_info['wireshark-windows-arm64']['stable'].update(get_installer_info('win64', installer))
        version_info['wireshark-windows-arm64']['stable'].update(get_signature_info(version_info['stable'], 'windows', 'arm64'))

        installer = f"Wireshark%20{version_info['stable']}%20Intel%2064.dmg"
        version_info['wireshark-macos-x86-64']['stable'].update(get_installer_info('osx', installer))
        version_info['wireshark-macos-x86-64']['stable'].update(get_signature_info(version_info['stable'], 'macos', 'intel64'))

        installer = f"Wireshark%20{version_info['stable']}%20Arm%2064.dmg"
        version_info['wireshark-macos-arm64']['stable'].update(get_installer_info('osx', installer))
        version_info['wireshark-macos-arm64']['stable'].update(get_signature_info(version_info['stable'], 'macos', 'arm64'))

        installer = f"Wireshark-win64-{version_info['oldstable']}.exe"
        version_info['wireshark-windows-x86-64']['oldstable'].update(get_installer_info('win64', installer))

        installer = f"Wireshark%20{version_info['oldstable']}%20Intel%2064.dmg"
        version_info['wireshark-macos-x86-64']['oldstable'].update(get_installer_info('osx', installer))
        version_info['wireshark-macos-x86-64']['oldstable'].update(get_signature_info(version_info['oldstable'], 'macos', 'intel64'))

        installer = f"Wireshark%20{version_info['oldstable']}%20Arm%2064.dmg"
        version_info['wireshark-macos-arm64']['oldstable'].update(get_installer_info('osx', installer))
        version_info['wireshark-macos-arm64']['oldstable'].update(get_signature_info(version_info['oldstable'], 'macos', 'arm64'))

        installer = f"Wireshark-win64-{version_info['oldoldstable']}.exe"
        version_info['wireshark-windows-x86-64']['oldoldstable'].update(get_installer_info('win64', installer))

        installer = f"Wireshark%20{version_info['oldoldstable']}%20Intel%2064.dmg"
        version_info['wireshark-macos-x86-64']['oldoldstable'].update(get_installer_info('osx', installer))
        version_info['wireshark-macos-x86-64']['oldoldstable'].update(get_signature_info(version_info['oldoldstable'], 'macos', 'intel64'))

        installer = f"Wireshark%20{version_info['oldoldstable']}%20Arm%2064.dmg"
        version_info['wireshark-macos-arm64']['oldoldstable'].update(get_installer_info('osx', installer))
        version_info['wireshark-macos-arm64']['oldoldstable'].update(get_signature_info(version_info['oldoldstable'], 'macos', 'arm64'))

        installer = f"Wireshark-{version_info['development']}-x64.exe"
        version_info['wireshark-windows-x86-64']['development'].update(get_installer_info('win64', installer))
        version_info['wireshark-windows-x86-64']['development'].update(get_signature_info(version_info['development'], 'windows', 'x64'))

        installer = f"Wireshark-{version_info['development']}-arm64.exe"
        version_info['wireshark-windows-arm64']['development'].update(get_installer_info('win64', installer))
        version_info['wireshark-windows-arm64']['development'].update(get_signature_info(version_info['development'], 'windows', 'arm64'))

        installer = f"Wireshark%20{version_info['development']}%20Intel%2064.dmg"
        version_info['wireshark-macos-x86-64']['development'].update(get_installer_info('osx', installer))
        version_info['wireshark-macos-x86-64']['development'].update(get_signature_info(version_info['development'], 'macos', 'intel64'))

        installer = f"Wireshark%20{version_info['development']}%20Arm%2064.dmg"
        version_info['wireshark-macos-arm64']['development'].update(get_installer_info('osx', installer))
        version_info['wireshark-macos-arm64']['development'].update(get_signature_info(version_info['development'], 'macos', 'arm64'))

        installer = f"Wireshark-win32-{version_info['oldoldstable']}.exe"
        version_info['wireshark-windows-x86']['oldoldstable'].update(get_installer_info('win32', installer))

    except SystemExit:
        return 1
    except:
        traceback.print_exc()
        return 1

    version_path = os.path.join(os.path.dirname(__file__), '..', 'src', 'components', 'Download', 'Versions.json')
    with open (version_path, 'w') as vj:
        json.dump(version_info, vj, sort_keys=True, indent=4)
        print('\nUpdated Versions.json')

    update_pad_xml()

    print()

    get_dfilter_list(version_info['stable'])
    get_dfilter_list(version_info['oldstable'])
    if version_info['oldoldstable']:
        get_dfilter_list(version_info['oldoldstable'])

    print()

    convert_release_note(version_info['stable'])
    convert_release_note(version_info['oldstable'])
    if version_info['oldoldstable']:
        convert_release_note(version_info['oldoldstable'])

    return 0

#
# On with the show
#

if __name__ == "__main__":
    sys.exit(main())
